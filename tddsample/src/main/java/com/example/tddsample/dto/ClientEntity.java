package com.example.tddsample.dto;
import java.util.Date;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name="clients",schema = "tddsample")
public class ClientEntity {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @Column(nullable = false, unique = true)
	    private String email;

	    @Column(nullable = false)
	    private String nom;

	    @Column(nullable = false)
	    private String prenom;

	    private String num_telephone;

	    @Temporal(TemporalType.DATE)
	    private Date date_naissance;

	    @ManyToOne
	    @JoinColumn(name = "sexe_id")
	    private SexeEntity sexe;
	    
	    @Column(columnDefinition = "boolean default true")
	    private boolean isActive;

		public ClientEntity(String email, String nom, String prenom, String num_telephone, Date date_naissance,
				SexeEntity sexe, boolean isActive) {
			super();
			this.email = email;
			this.nom = nom;
			this.prenom = prenom;
			this.num_telephone = num_telephone;
			this.date_naissance = date_naissance;
			this.sexe = sexe;
			this.isActive = isActive;
		}




}



