package com.example.tddsample.service;


import com.example.tddsample.dao.IClientRepository;
import com.example.tddsample.dto.ClientEntity;
import com.example.tddsample.dto.Sexe;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientsService implements IClientsService {
    private final IClientRepository iClientRepository;

    public ClientsService(IClientRepository iClientRepository) {
        this.iClientRepository = iClientRepository;
    }


    @Override
    public boolean addClients(ClientEntity client) throws Exception {

        try{
           iClientRepository.save(client);
            return true;
        } catch (DataAccessException e){

            throw new Exception("Erreur lors de l'ajout  du client " + e.getMessage());
        }

    }

    @Override
    public List<ClientEntity> getAllClients() throws Exception {
        try{
            return iClientRepository.findAll();
        } catch (DataAccessException e){

            throw new Exception("erreur lors de la recuperation de la liste des clients " + e.getMessage());
        }

    }

    @Override
    public Optional<ClientEntity> getClientById(Long id) throws Exception {
        try{
            return iClientRepository.findById(id);
        } catch (DataAccessException e){

            throw new Exception("erreur lors de la recuperation d'un utilisateur via son Id " + e.getMessage());
        }
    }



    @Override
    public ClientEntity getClientByEmail(String email) throws Exception {
        try{
            return iClientRepository.findClientByEmail(email);
        } catch (DataAccessException e){

            throw new Exception("erreur lors de la recuperation d'un utilisateur via son Id " + e.getMessage());
        }
    }


    @Override
    public List<ClientEntity> getClientsBySexe(Sexe sexe) throws Exception {
        throw new Exception("not implemented");
    }

    @Override
    public boolean removeClient(Long id) throws Exception {
        throw new Exception("not implemented");
    }

    @Override
    public boolean desactivatedClient(Long id) throws Exception {
        throw new Exception("not implemented");
    }

    @Override
    public boolean modifyClient(ClientEntity clientModified) throws Exception {
        throw new Exception("not implemented");
    }



}