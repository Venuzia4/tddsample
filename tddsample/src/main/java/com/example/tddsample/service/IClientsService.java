package com.example.tddsample.service;

import java.util.List;

import com.example.tddsample.dto.ClientEntity;
import com.example.tddsample.dto.Sexe;

import java.util.List;
import java.util.Optional;

public interface IClientsService {

	/**
	 * Ajouter un client ou une liste de clients
	 * @param client: client à ajouter
	 * @return boolean succès de l'ajout
	 * @throws Exception 
	 */
	boolean addClients(ClientEntity client) throws Exception;
	
	/**
	 * Sélectionner l’ensemble des clients
	 * @return liste de tout les clients 
	 * @throws Exception 
	 */
	List<ClientEntity> getAllClients() throws Exception;
	
	/**
	 * Chercher un client par identifiant
	 *
	 * @param id: id du client cherché
	 * @return client à l'id correspondant à celui passer en paramètre
	 * @throws Exception
	 */

	Optional<ClientEntity> getClientById(Long id) throws Exception;

	/**
	 * Chercher un client par email
	 * @param email: email du client cherché
	 * @return client à l'email correspondant à celui passer en paramètre
	 * @throws Exception 
	 */

	ClientEntity getClientByEmail(String email) throws Exception;

	/**
	 * Chercher l’ensemble des clients par sexe.
	 * @param sexe: sexe par lequel on veut trier
	 * @return liste des clients du sexe passé en paramètre
	 * @throws Exception 
	 */
	List<ClientEntity> getClientsBySexe(Sexe sexe) throws Exception;
	
	/**
	 * Supprimer un client
	 * @param  id: id du client à supprimer
	 * @return boolean du succès
	 * @throws Exception 
	 */
	boolean removeClient(Long id) throws Exception;
	
	/**
	 * Désactiver un client
	 * @param id: id du client à désactiver
	 * @return boolean du succès
	 * @throws Exception 
	 */
	boolean desactivatedClient(Long id) throws Exception;
	
	/**
	 * Modifier un client.
	 * @param clientModified: nouvel valeur du client
	 * @return boolean du succès
	 * @throws Exception 
	 */
	boolean modifyClient(ClientEntity clientModified) throws Exception;
}
