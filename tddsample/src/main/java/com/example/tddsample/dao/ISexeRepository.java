package com.example.tddsample.dao;

import com.example.tddsample.dto.Sexe;
import com.example.tddsample.dto.SexeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISexeRepository extends JpaRepository <SexeEntity,Long> {
 public SexeEntity findByName(Sexe name );
}
