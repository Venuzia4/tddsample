package com.example.tddsample.dao;

import com.example.tddsample.dto.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IClientRepository  extends JpaRepository<ClientEntity,Long> {


  public  Optional <ClientEntity> findAllById(Long id);

    ClientEntity findClientByEmail(String email);
}
