package com.example.tddsample.controller;

import com.example.tddsample.dto.ClientEntity;
import com.example.tddsample.service.IClientsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@Slf4j
@RestController
@RequestMapping("/api")
public class ClientController {
    @Autowired
    private IClientsService iClientsService;

    @GetMapping("/clients")
    public List<ClientEntity> getAllClients() throws Exception {
        return iClientsService.getAllClients();
    }

    @GetMapping("/client/{id}")
    public Optional<ClientEntity> getClientById(@PathVariable ClientEntity id ) throws Exception {
        throw new Exception("not implemented");
    }

}
