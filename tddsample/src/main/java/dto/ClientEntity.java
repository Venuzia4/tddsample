package dto;
import java.util.Date;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name="clients",schema = "tddsample")
public class ClientEntity {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @Column(nullable = false, unique = true)
	    private String email;

	    @Column(nullable = false)
	    private String nom;

	    @Column(nullable = false)
	    private String prenom;

	    private String num_telephone;

	    @Temporal(TemporalType.DATE)
	    private Date date_naissance;

	    private String sexe;

	    @Column(columnDefinition = "boolean default true")
	    private boolean isActive;



}



