package com.example.tddsample.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.hamcrest.Matchers.*;

import org.aspectj.lang.annotation.Before;

class ClientControllerTest {
	
	private MockMvc mockMvc;
	
	@BeforeEach
	public void setup() {
		ClientController clientController = new ClientController();
		mockMvc = MockMvcBuilders.standaloneSetup(clientController).build();
	}

    @Test
    void getAllClients() throws Exception{
    	// Exécutez la requête GET pour la route /clients/
        mockMvc.perform(get("/api/clients"))
                .andExpect(status().isOk()) // Vérifiez que le statut de la réponse est OK (200)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)) // Vérifiez que le contenu est de type JSON
                .andExpect(jsonPath("$", hasSize(3))) // Vérifiez que la réponse contient deux éléments (par exemple)
                .andExpect(jsonPath("$[0].id", is(1))) // Vérifiez une propriété spécifique du premier élément (par exemple)
                .andExpect(jsonPath("$[0].name", is("Aaa"))) // Vérifiez une autre propriété spécifique (par exemple)
                .andExpect(jsonPath("$[1].id", is(2))) // Vérifiez une propriété du deuxième élément (par exemple)
                .andExpect(jsonPath("$[1].name", is("Bbb"))); // Vérifiez une autre propriété du deuxième élément (par exemple)
    }

    @Test
    void getClientById() {
    }
}