package com.example.tddsample.service;


import com.example.tddsample.dao.IClientRepository;
import com.example.tddsample.dao.ISexeRepository;
import com.example.tddsample.dto.ClientEntity;
import com.example.tddsample.dto.Sexe;
import com.example.tddsample.dto.SexeEntity;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-test.properties")
class ClientsServiceTest {
	
	@Autowired
	private ClientsService clientsService;


    @Autowired
    private IClientRepository iClientRepository;

    private List<ClientEntity> clients;
    
    @BeforeAll
    void initialize() {
    	clients = new ArrayList<>();
    	clients.add(new ClientEntity("aaa.aa@gmail.com", "aaa", "aa", "0601020304", new Date(2000, 1, 1), new SexeEntity(1, Sexe.MASCULIN), true));
    	clients.add(new ClientEntity("bbb.bb@gmail.com", "bbb", "bb", "0601020304", new Date(2000, 1, 1), new SexeEntity(1, Sexe.FEMININ), true));
    	clients.add(new ClientEntity("ccc.cc@gmail.com", "ccc", "cc", "0601020304", new Date(2000, 1, 1), new SexeEntity(1, Sexe.MASCULIN), true));
    }

    @Test
    void addClients() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ClientEntity client = new ClientEntity();
        client.setNom("Mabika");
        client.setPrenom("venuzia");
        client.setEmail("venuzia@gmail.com");
        client.setNum_telephone("0650732651");
        client.setDate_naissance(dateFormat.parse("2000-02-02"));
        client.setActive(true);
        client.setSexe(new SexeEntity(1, Sexe.MASCULIN));
        clientsService.addClients(client);
        assertNotNull(client.getId());

    }

    @Test
    void getAllClients() throws Exception {

        SexeEntity sexe1 = new SexeEntity();
        sexe1.setId(1);
        sexe1.setName(Sexe.valueOf("MASCULIN"));
        SexeEntity sexe2 = new SexeEntity();
        sexe2.setId(2);
        sexe2.setName(Sexe.valueOf("FEMININ"));

        ClientEntity client1 = new ClientEntity();
        client1.setId(1L);
        client1.setNom("test");
        client1.setSexe(sexe1);

        ClientEntity client2 = new ClientEntity();
        client2.setId(2L);
        client2.setNom("test2");
        client2.setSexe(sexe2);

        List<ClientEntity> clientEntityList = clientsService.getAllClients();
        clientEntityList.add(client1);
        clientEntityList.add(client2);
        assertEquals(2,clientEntityList.size());
        assertEquals(client1.getId(),clientEntityList.get(0).getId());
        assertEquals(client1.getNom(),clientEntityList.get(0).getNom());
        assertEquals(client2.getId(),clientEntityList.get(1).getId());
        assertEquals(client2.getNom(),clientEntityList.get(1).getNom());
    }

    @Test
    void getClientById() throws Exception {
        SexeEntity sexe1 = new SexeEntity();
        sexe1.setId(1);
        sexe1.setName(Sexe.valueOf("MASCULIN"));
        SexeEntity sexe2 = new SexeEntity();
        sexe2.setId(2);
        sexe2.setName(Sexe.valueOf("FEMININ"));

        ClientEntity client1 = new ClientEntity();
        client1.setId(1L);
        client1.setNom("test");
        client1.setPrenom("toto");
        client1.setEmail("test@example.com");
        client1.setSexe(sexe1);
        iClientRepository.save(client1);

        ClientEntity client2 = new ClientEntity();
        client2.setId(2L);
        client2.setNom("test2");
        client2.setPrenom("toto");
        client2.setEmail("test2@example.com");
        client2.setSexe(sexe2);
        iClientRepository.save(client2);

        Optional<ClientEntity> optionalClient = clientsService.getClientById(client1.getId());
        assertTrue(optionalClient.isPresent());
        ClientEntity retrievedClient =optionalClient.get();
        assertEquals(client1.getId(),retrievedClient.getId());


    }

    @Test
    void getClientByEmail() throws Exception {
        SexeEntity sexe1 = new SexeEntity();
        sexe1.setId(1);
        sexe1.setName(Sexe.valueOf("MASCULIN"));
        SexeEntity sexe2 = new SexeEntity();
        sexe2.setId(2);
        sexe2.setName(Sexe.valueOf("FEMININ"));

        ClientEntity client1 = new ClientEntity();
        client1.setId(1L);
        client1.setNom("test");
        client1.setPrenom("toto");
        client1.setEmail("test@example.com");
        client1.setSexe(sexe1);
        iClientRepository.save(client1);
        ClientEntity client2 = new ClientEntity();
        client2.setId(2L);
        client2.setNom("test2");
        client2.setPrenom("toto");
        client2.setEmail("test2@example.com");
        client2.setSexe(sexe2);
        iClientRepository.save(client2);
        Optional<ClientEntity> optionalClient = Optional.ofNullable(clientsService.getClientByEmail(client1.getEmail()));
        assertTrue(optionalClient.isPresent());
        ClientEntity retrievedClient =optionalClient.get();
        assertEquals(client1.getId(),retrievedClient.getId());

    }

    @Test
    void getClientsBySexe() throws Exception {
    	List<ClientEntity> clientsM = clientsService.getClientsBySexe(Sexe.MASCULIN);
    	List<ClientEntity> clientsF = clientsService.getClientsBySexe(Sexe.FEMININ);

    	
    	assertFalse(clientsM.isEmpty());
    	assertFalse(clientsF.isEmpty());
    	assertEquals(clientsM.size(), 2);
    	assertEquals(clientsF.size(), 1);

    	List<ClientEntity> clientsResult = new ArrayList<ClientEntity>(clients);
    	
    	assertEquals(clientsM.get(0), clientsResult.get(0));
    	assertEquals(clientsM.get(1), clientsResult.get(2));
    	assertEquals(clientsF.get(0), clientsResult.get(1));
    }

    
    @Test
    void removeClient() throws Exception {
    	List<ClientEntity> clientsResult = new ArrayList<ClientEntity>(clients);

    	//Cas le client n'existe pas (null et mauvais id)
    	assertFalse(clientsService.removeClient(null));
    	assertFalse(clientsService.removeClient(15L));
    	//Cas le client est supprimé
    	assertTrue(clientsService.removeClient(1L));
    	//assertThrows(EntityNotFoundException , iClientRepository.ge .getById(1L)));
    	
    	
    }

    @Test
    void desactivatedClient() throws Exception {    	
    	List<ClientEntity> clientsResult = new ArrayList<ClientEntity>(clients);
    	
    	boolean result = clientsService.desactivatedClient(null);

    	//Cas le client est activé et devient désactivé
    	//Cas le client est désactivé et reste désactivé
    	
    	//Cas le client est désactivé et devient activé
    	//Cas le client est activé et reste activé
    	
    }

    @Test
    void modifyClient() throws Exception {    	
    	List<ClientEntity> clientsResult = new ArrayList<ClientEntity>(clients);

    	boolean result = clientsService.modifyClient(null);
    	
    	//Cas on ne modifie rien
    	//Cas le client n'existe pas
    	//Cas on modifie un champ
    	//Cas on modifie plusieurs champs
    	//Cas on modifie un champ unique 
    }
}