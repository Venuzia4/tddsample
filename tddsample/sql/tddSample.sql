-- Host: localhost    Database: tddSample
-- ------------------------------------------------------
use tddSample;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL UNIQUE,
  `nom` varchar(55) NOT NULL,
  `prenom` varchar(55) NOT NULL,
  `num_telephone` varchar(25),
  `date_naissance` date,
  `sexe` varchar(25),
  `is_active` bool DEFAULT true,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `sexe` (
  `id_sexe` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`id_sexe`)
);

ALTER TABLE `clients`
  ADD COLUMN `id_sexe` bigint(20) NOT NULL,
  ADD FOREIGN KEY (`id_sexe`) REFERENCES `sexe` (`id_sexe`);


--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
UNLOCK TABLES;

